/*
 * CircularBuffer.c
 *
 *  Created on: 28 Nov 2016
 *      Author: asolis
 */

#include "CircularBuffer.h"

/**
 * Returns a list of size listSize of uint8_t
 */
uint8_t* getList(uint8_t listSize) {
	uint8_t *myList = malloc(sizeof(uint8_t) * listSize);
	return myList;
}

/**
 * Factory for a Buffer
 */
Buffer* getBuffer(uint8_t bufferSize) {
	uint8_t *myBuffer = getList(bufferSize);
	Buffer *aBuffer = malloc(sizeof(struct Buffer));

	aBuffer->size = bufferSize;
	aBuffer->buffer = myBuffer;
	aBuffer->bufferEnd = aBuffer->buffer + bufferSize;

	aBuffer->count = 0;
	aBuffer->head = myBuffer;
	aBuffer->tail = myBuffer;
	return aBuffer;
};

/**
 * Updates the pointer from the buffer by the amount of steps passed
 */
Buffer* updatePointer(Buffer* buffer, int targetPointer, int step, int counterStep) {
	uint8_t** pointer = (targetPointer == head) ? &(buffer->head) : &(buffer->tail);
	*pointer = (*pointer == buffer->bufferEnd) ? buffer->buffer : *pointer + step;
	buffer->count = buffer->count + counterStep;  // updates the count
	return buffer;
}

/**
 * Inserts the data to the buffer and returns the data inserted
 */
uint8_t insert(Buffer* buffer, uint8_t data) {
	if (buffer->count >= buffer->size  || (buffer->head == buffer->tail && buffer->count >= buffer->size)) {
		// head and tail are the same and it has contents -> error
		return 0x00;
	}
	memcpy(buffer->head, &data, sizeof(uint8_t));
	uint8_t insertedData = *(buffer->head);
	buffer = updatePointer(buffer, head, 1, 1);
	return insertedData;
}

/**
 * Pops the current element from the buffer
 */
uint8_t pop(Buffer* buffer) {
	if (buffer->count == 0 || buffer->tail == buffer->head) {
		// error, initial condition, tail can't pass the head!
		return 0x00;
	}
	uint8_t dataToReturn = *(buffer->tail);
	buffer = updatePointer(buffer, tail, 1, -1);
	return dataToReturn;
};

/**
 * Calculates the size of the buffer and returns it
 */
uint8_t count(Buffer* buffer) {
	return buffer->count;
}

/**
 * Returns an integer value on whether the buffer is full or not
 */
int isFull(Buffer* buffer) {
	if (buffer->count == 0 && buffer->size == 0) return 0;
	return(buffer->count == buffer->size);
}

/**
 * Frees the memory from the buffer
 */
Buffer* cleanBuffer(Buffer* buffer) {
	free(buffer->buffer);
	buffer->size = -1;
	buffer->count = 0;
	return buffer;
}

/**
 * Cleans up all the Buffer's instances from the memory
 */
Buffer* cleanUp(Buffer* bufferToClean) {
	if (bufferToClean->size > -1) {
		free(bufferToClean->buffer);
	}
	free(bufferToClean);
	return bufferToClean;
}

/**
 * Making the CircularBuffer Functions constants
 */
const struct CircularBuffer CircularBuffer = {
		.getBuffer = getBuffer,
		.getList = getList,
		.pop = pop,
		.insert = insert,
		.count = count,
		.isFull = isFull,
		.updatePointer = updatePointer
};

/**
 * Map the functions to the Janitor
 */
const struct Janitor Janitor = {
		.cleanUp = cleanUp,
		.cleanBuffer = cleanBuffer
};
