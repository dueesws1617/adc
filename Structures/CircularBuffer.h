/*
 * CircularBuffer.h
 *
 *  Created on: 28 Nov 2016
 *      Author: asolis
 */

#ifndef STRUCTURES_CIRCULARBUFFER_H_
#define STRUCTURES_CIRCULARBUFFER_H_

#include <avr/io.h>
#include <inttypes.h>

#ifndef NULL
#define NULL (void *)0
#endif

/**
 * Enumeration
 */
enum targetPointer {
	head,
	tail
};

/**
 * Structure to hold a Buffer
 */
typedef struct Buffer {
	uint8_t *buffer; // actual buffer
	uint8_t *bufferEnd; // points to the end of the buffer
	uint8_t *head; // pointer to the head of the buffer
	uint8_t *tail; // pointer to the end of the buffer
	uint8_t count; // counter holding the amount of elements
	uint8_t size;  // Size of the buffer
} Buffer;

/**
 * Circular Buffer methods
 */
struct CircularBuffer {
	Buffer* (*getBuffer)(uint8_t);
	uint8_t* (*getList)(uint8_t);
	uint8_t (*pop)(Buffer*);
	uint8_t (*insert)(Buffer*, uint8_t);
	uint8_t (*count)(Buffer*);
	int (*isFull)(Buffer*);
	void (*updatePointer)(Buffer*, int, int, int);
};

/**
 * Janitor structure that handles all the cleaning of memory
 */
struct Janitor {
	Buffer* (*cleanUp)(Buffer*);
	Buffer* (*cleanBuffer)(Buffer*);
};

extern const struct CircularBuffer CircularBuffer;
extern const struct Janitor Janitor;

#endif /* STRUCTURES_CIRCULARBUFFER_H_ */
