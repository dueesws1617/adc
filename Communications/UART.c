#include "UART.h"

#define NEW_LINE 0xA
#define CR 0xD

/**
 * Flushes the buffer
 */
void flush() {
	while(UCSR0A & (1 << RXC0)) {
		unsigned char dummy = UDR0;
	}
}

/**
 * Initializes the UART as a 8N1 Channel Format
 */
void init(uint16_t baudRate, int clear) {

	uint16_t final_baud_val;

	//calculate BAUD Rate
	final_baud_val = F_CPU / (baudRate * 16L) - 1;
	UBRR0H = (uint8_t) (final_baud_val >> 8);
	UBRR0L = (uint8_t) final_baud_val;

	//Enable receiving and sending
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	//use 8N1 (bit/n-parity/1stop-bit) UART format
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);

	if (clear == 1) flush();

}

/**
 * appends a character in the bus
 */
void append(char character) {

	//wait until UART is ready to send
	while (!(UCSR0A & (1 << UDRE0)));
	//set UDR0 to the value to send
	UDR0 = character;

}

/**
 * Writes a fresh new line
 */
void newLine() {
	UART.append(NEW_LINE);
};

/**
 * Gets one character from the bus
 */
uint8_t get() {
	//wait until byte available on bus
	while (!(UCSR0A & (1 << RXC0)));
	//return that byte
	return UDR0;
}

/**
 * Reads a sentence
 */
uint16_t read(char* someString) {
	//Read bytes until newline occurs
	uint16_t count = 0;
	char charReceived;
	do {
		charReceived = UART.get();
		*someString = charReceived;
		someString++;
		count++;
	} while (charReceived != 0x0A);
	//return number of read bytes (including newline)
	return count;
}

/**
 * Writes a series of numbers (as characters)
 */
void writeNumbers(int32_t number) {
	char buf[20];
	sprintf(buf, "0x%x", number);
	UART.write(buf);
}

/**
 * Writes a series of characters, depends on append
 */
void write(char* someString) {
	while (*someString) {
		UART.append(*someString);
		someString++;
	}
}

/* definition of our structure */
const struct UART UART = {
		.flush = flush,
		.init = init,
		.append = append,
		.newLine = newLine,
		.read = read,
		.get = get,
		.writeNumbers = writeNumbers,
		.write = write
};
