/*
 * uart_communication.h
 *
 *  Created on: 14 Nov 2016
 *      Author: asolis
 */

#ifndef COMMUNICATIONS_UART_H_
#define COMMUNICATIONS_UART_H_

#include <avr/io.h>
#include <stdio.h>
#include <inttypes.h>

/* UART Library holder */
struct UART {
	void (*flush)();
	void (*init)(uint16_t baudRate, int clear);
	void (*append)(char character);
	uint8_t (*get)(void);
	uint16_t (*read)(char *s);
	void (*writeNumbers)(int32_t n);
	void (*write)(char* s);
	void (*newLine)();
};
extern const struct UART UART;  // Expose the structure

#endif /* COMMUNICATIONS_UART_H_ */
