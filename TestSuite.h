/*
 * TestSuite.h
 */

#ifndef TESTSUITE_H_
#define TESTSUITE_H_

#include <stdio.h>
#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "System.h"
#include "uCUnit-v1.0.h"


#include "Sensors/Temperature.h"
#include "Communications/UART.h"
#include "Structures/CircularBuffer.h"
#include "Persistance/EEPROM.h"

#define ASCII_LOW 0x00
#define ASCII_HIGH 0x7F

int TestSuite_run();

#endif /* TESTSUITE_H_ */
