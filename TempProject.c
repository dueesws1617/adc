#include <avr/io.h>
#include "TestSuite.h"

/**
 * Main function
 */
int main(void) {
	// just simply run the tests
	return TestSuite_run();
}
