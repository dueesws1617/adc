/*
 * EEPROM.h
 *
 *  Created on: 22 Jan 2017
 *      Author: asolis
 */

#ifndef PERSISTANCE_EEPROM_H_
#define PERSISTANCE_EEPROM_H_

#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <inttypes.h>

typedef struct EepromSlave {
	uint8_t baseAddress;
	uint8_t tail;
} EepromSlave;

/**
 * http://www.nongnu.org/avr-libc/user-manual/group__avr__eeprom.html
 */
struct EEPROM {
	uint8_t (*read)(EepromSlave*, uint8_t);
	void (*write)(EepromSlave*, uint8_t);
	uint8_t (*readByte)(uint8_t*);
	uint16_t (*readWord)(uint16_t*);
	void (*writeByte)(uint8_t*, uint8_t);
	void (*writeWord)(uint8_t*, uint8_t);
	void (*alloc)(EepromSlave*, uint8_t);
	uint8_t (*update)(EepromSlave*, uint8_t, uint8_t);
};

extern const struct EEPROM EEPROM;

#endif /* PERSISTANCE_EEPROM_H_ */
