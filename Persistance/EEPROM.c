/*
 * EEPROM.c
 *
 *  Created on: 22 Jan 2017
 *      Author: asolis
 */

#include "EEPROM.h"

/**
 * Reads from the eeprom in an specific address
 */
uint8_t doEepromReadByte(uint8_t *addr) {
	eeprom_busy_wait();
	cli();
	uint8_t read = eeprom_read_byte(addr);
	sei();
	return read;
}

/**
 * Reads a word
 */
uint16_t doEepromReadWord(uint16_t *addr) {
	eeprom_busy_wait();
	cli();
	uint16_t read = eeprom_read_word(addr);
	sei();
	return read;
}

/**
 * Writes a Byte
 */
void doEepromWriteByte(uint8_t* address, uint8_t data) {
	eeprom_busy_wait();
	cli();
	eeprom_write_byte(address, data);
	sei();
}

/**
 * Writes 2 bytes at once
 */
void doEepromWriteWord(uint16_t *address, uint16_t data) {
	eeprom_busy_wait();
	cli();
	eeprom_write_word(address, data);
	sei();
}

/**
 * Reads relative from a slave
 */
uint8_t readFrom(EepromSlave* slave, uint8_t offset) {
	return doEepromReadByte((uint8_t*)(slave->baseAddress + offset));
}

/**
 * Writes based on the slave
 */
uint8_t writeBasedOn(EepromSlave* slave, uint8_t data) {
	uint8_t abs_position = slave->baseAddress + slave->tail;
	doEepromWriteByte((uint8_t*) abs_position, data);
	slave->tail++;
	return slave->tail - 0x1;
}

uint8_t updateBasedOn(EepromSlave* slave, uint8_t offset, uint8_t data) {
	uint8_t abs_position = slave->baseAddress + offset;
	doEepromWriteByte((uint8_t*) abs_position, data);
	return abs_position;
}

/**
 * allocates memory in eeprom as 0x0 for the amount of bytes passed
 */
void allocateMemory(EepromSlave *slave, uint8_t byteAmount) {
	uint8_t dummy = 0x00;

	if (slave->baseAddress == 0x0) {
		slave->baseAddress = 0x1;
	}

	while (byteAmount > 0x0) {
		writeBasedOn(slave, dummy);
		byteAmount--;
	}
}


/**
 * mapping to functions
 */
const struct EEPROM EEPROM = {
		.read = readFrom,
		.write = writeBasedOn,
		.readByte = doEepromReadByte,
		.writeByte = doEepromReadByte,
		.readWord = doEepromReadWord,
		.writeWord = doEepromWriteWord,
		.alloc = allocateMemory,
		.update = updateBasedOn
};
