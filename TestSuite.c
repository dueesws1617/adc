/*
 * TestSuite.c
 *
 *  Created on: 22 Jan 2017
 *      Author: asolis
 */

#include "TestSuite.h"

Buffer *myBuffer;
EepromSlave *eepromSlave;

/**
 * Prints the tests
 */
void printFootPrint() {
	UCUNIT_WriteString("\n********");
	UCUNIT_WriteString("\nName: ");
	UCUNIT_WriteString("Temperature");
	UCUNIT_WriteString("\nCompiled: ");
	UCUNIT_WriteString(__DATE__);
	UCUNIT_WriteString("\nTime: ");
	UCUNIT_WriteString(__TIME__);
	UCUNIT_WriteString("\nVersion:  ");
	UCUNIT_WriteString(UCUNIT_VERSION);
	UCUNIT_WriteString("\n********\n");
}

/**
 * Interrupt in timer1 that activates every ~ 1 second
 */
ISR(TIMER1_COMPA_vect) {
	DDRB |= (1 << PB5); // lets set the PORTB as output
	PORTB ^= 1 << 5;
	UART.write("-----------------------\n");
	Temperature.setUp();
	Temperature.start();
}

int identificator = 0;

/**
 * Interrupt for the ADC read complete
 */
ISR(ADC_vect) {
	// conversion complete --> get data, print data
	identificator++;
	UART.writeNumbers(identificator);
	UART.write("Conversion complete \n");

	uint16_t rawTemp = ADCW;  // gets the word of the ADC conversion result
	uint8_t rawL = ADCL;
	uint8_t rawH = ADCH;

	UART.writeNumbers(rawTemp);
	UART.newLine();
	UART.write("Temp: ");
	double t = (rawTemp - 324.31) / 1.22;
	UART.writeNumbers(t);
	UART.newLine();

	if (myBuffer != NULL) {
		UART.write("Will add to CB");
		UART.newLine();
		CircularBuffer.insert(myBuffer, rawH);
		CircularBuffer.insert(myBuffer, rawL);
		UART.write("Added to CB");
		UART.newLine();
	}

	if (identificator >= 10) {
		// theoretical add 5 to the EEPROM
		uint8_t me = 0x1;
		while (identificator > 5) {
			UART.write("Will save to eeprom");
			UART.newLine();
			uint8_t first = CircularBuffer.pop(myBuffer);
			uint8_t second = CircularBuffer.pop(myBuffer);
			EEPROM.update(eepromSlave, me, first);
			EEPROM.update(eepromSlave, me + 0x1, second);
			identificator--;
			UART.write("Saved to eeprom: ");
			UART.newLine();
			UART.writeNumbers(first);
			UART.newLine();
			UART.writeNumbers(second);
			UART.newLine();
		}
	}
}

//EEPROM tests
void test_ReadFromEEPROM(EepromSlave *slave) {
	UCUNIT_TestcaseBegin("test slave i/o");
	uint8_t dummy = 0x15;
	EEPROM.write(slave, dummy);
	uint8_t dummy2 = EEPROM.read(slave, slave->tail - 0x1);
	UCUNIT_CheckIsEqual(dummy, dummy2);
	UCUNIT_TestcaseEnd();
}


int TestSuite_run() {
	UCUNIT_Init();  // init the ucunit test library
	printFootPrint();
	myBuffer = CircularBuffer.getBuffer(100);
	EepromSlave *slave = malloc(sizeof(EepromSlave));
	EEPROM.alloc(slave, 10);

	do {
		wdt_reset();
		//test_ReadFromEEPROM(slave);
		//UCUNIT_WriteSummary();
	}while (1);

	return 0;
}
