/*****************************************************************************
 *                                                                           *
 *  uCUnit - A unit testing framework for microcontrollers                   *
 *                                                                           *
 *  (C) 2007 - 2008 Sven Stefan Krauss                                       *
 *                  https://www.ucunit.org                                   *
 *                                                                           *
 *  File        : System.c                                                   *
 *  Description : System dependent functions used by uCUnit.                 *
 *                This file has to be customized for your hardware.         *
 *  Author      : Sven Stefan Krauss                                         *
 *  Contact     : www.ucunit.org                                             *
 *                                                                           *
 *****************************************************************************/

/*
 * This file is part of ucUnit.
 *
 * You can redistribute and/or modify it under the terms of the
 * Common Public License as published by IBM Corporation; either
 * version 1.0 of the License, or (at your option) any later version.
 *
 * uCUnit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Common Public License for more details.
 *
 * You should have received a copy of the Common Public License
 * along with uCUnit.
 *
 * It may also be available at the following URL:
 *       http://www.opensource.org/licenses/cpl1.0.txt
 *
 * If you cannot obtain a copy of the License, please contact the
 * author.
 */

#include "System.h"
#include <avr/delay.h>


void setUpWatchDog() {
	wdt_reset();
	cli();
	//MCUSR &= ~(1 << WDRF);  // because the guide said to
	WDTCSR |= (_BV(WDCE) | _BV(WDE)); // enter WD configuration
	WDTCSR |= (_BV(WDE) | _BV(WDIE)); // enabled wtd every 2 seconds
	wdt_enable(WDTO_1S);
	sei();
}

/**
 * This sets a timer for every second (turns on and off the led)
 * http://www.instructables.com/id/Arduino-Timer-Interrupts/?ALLSTEPS
 */
void setUpLed() {
	// initialize Timer1
	cli();             // disable global interrupts
	//set timer1 interrupt at 1Hz
	TCCR1A = 0x0;// set entire TCCR1A register to 0
	TCCR1B = 0x0;// same for TCCR1B
	TCNT1  = 0x0;//initialize counter value to 0
	// set compare match register for 1hz increments
	OCR1A = 0x3D08;// = (16*10^6) / (1*1024) - 1 (must be <65536)
	// turn on CTC mode
	TCCR1B |= (1 << WGM12);
	// Set CS10 and CS12 bits for 1024 prescaler
	TCCR1B |= (1 << CS12) | (1 << CS10);
	// enable timer compare interrupt
	TIMSK1 |= (1 << OCIE1A);
	sei();
}

/* Initialize your hardware */
void System_Init(void)
{
	UART.init(BAUD, true);
	// set up the watch dog
	setUpWatchDog();
	// set up the LED as outout
	setUpLed();
}

/* Shutdown your hardware */
void System_Shutdown(void)
{
	exit(0);
}

/* Resets the system */
void System_Reset(void)
{
	UART.flush();
	exit(0);
}

/* Stub: Put system in a safe state */
void System_Safestate(void)
{

	exit(0);
}


/* Stub: Transmit a string to the host/debugger/simulator */
void System_WriteString(char * s)
{
	//sprintf("Tests: %s", s);
	UART.write(s);
}

void System_WriteInt(int n)
{
	UART.writeNumbers(n);
}

/**
 * Register the watchdog interrupt
 */
ISR(WDT_vect) {
	// do something with the interrupt
	wdt_reset();  // reset once the timeout runs out
}
