import serial
import settings
import logging
import colorlog

ser = None

FORMAT = "%(asctime)s - %(levelname)s: %(message)s"
# format=FORMAT, datefmt="%d/%m/%Y %H:%M:%S"

def print_footprint():

    colorlog.basicConfig(level=logging.INFO)
    colorlog.info("LoopBack Helper Test for TestSuite: CircularBuffer")
    colorlog.info("USB port: {usb_port}".format(usb_port=settings.USB_PORT))
    colorlog.info("BAUD Rate: {baud_rate}".format(baud_rate=settings.BAUD_RATE))

try:
    print_footprint()
    ser = serial.Serial(
        timeout=None,
        port=settings.USB_PORT,
        baudrate=settings.BAUD_RATE,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS
    )
    colorlog.info("Resetting serial channel")
    ser.close()
    ser.open()
    while 1:  # prints all incoming lines
        gotten = ser.readline()
        if "fail" in gotten and "../" in gotten:
            colorlog.error(gotten)
        elif "failed: " in gotten:
            colorlog.critical(gotten)
        else:
            colorlog.info(gotten)
        if "shutdown" in gotten:
            break
    colorlog.info("Test finished successfully")
except KeyboardInterrupt:
    colorlog.warning("Hot exit")
except (OSError, IOError, serial.serialutil.SerialException) as e:
    colorlog.error(str(e))
finally:
    if ser:
        ser.close()
        colorlog.warning("Closed the serial channel")
