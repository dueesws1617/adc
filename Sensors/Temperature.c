/*
 * Temperature.c
 *
 *  Created on: 22 Jan 2017
 *      Author: asolis
 */

#include "Temperature.h"

/**
 * returns the temperature
 */
void setUpInterrupt() {
	cli(); // disable interrupts
	ADMUX = (_BV(REFS1) | _BV(REFS0) | _BV(MUX3)); // use internal voltage reference
	// also tell to use the temperature sensor

	ADCSRA |= _BV(ADEN);  // enabled the ADC
	// set ADSC to start the conversion! (Single mode)
	ADCSRA |= _BV(ADIF); // enable the interrupt flag??
	ADCSRA |= _BV(ADIE); // enable the interrupt
	ADCSRA |= (_BV(ADPS2) | _BV(ADPS1)); // enabled the prescaler
	// has a prescaler of factor 64
	sei(); //enable interrupts
}

/**
 * Sets the ADSC bit to start sampling
 */
void startSampling() {
	ADCSRA |= _BV(ADSC);
}

/**
 * http://www.avrfreaks.net/forum/328p-internal-temperature
 */
double getTemperature() {
	uint16_t wADC;
	double t;

	ADMUX = (_BV(REFS1) | _BV(REFS0) | _BV(MUX3)); // use internal voltage reference
	ADCSRA |= (_BV(ADPS2) | _BV(ADPS1)); // enabled the prescaler
	// factor 64
	ADCSRA |= _BV(ADEN);  // enabled the ADC
	_delay_ms(20); // wait for the voltage to become stable

	ADCSRA |= _BV(ADSC); // starts the ADC

	//wait until the end-of-conversion
	while(!(ADCSRA & (1 << ADSC)));

	wADC = ADCW; // difference between ADCW -> Word (16 bits)
	// ADCL --> reads the lower 8 bits of the 16 bits
	// ADCH --> reads the higher 8 bits of the 16 bits

	t = (wADC - 324.31) / 1.22;

	return t;
}

/**
 * Interface
 */
const struct Temperature Temperature = {
		.get = getTemperature,
		.start = startSampling,
		.setUp = setUpInterrupt
};
