/*
 * Temperature.h
 *
 *  Created on: 22 Jan 2017
 *      Author: asolis
 */

#ifndef SENSORS_TEMPERATURE_H_
#define SENSORS_TEMPERATURE_H_

#include <avr/io.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/delay.h>

struct Temperature {
	double (*get)();
	void (*start)();
	void (*setUp)();
};

extern const struct Temperature Temperature;

#endif /* SENSORS_TEMPERATURE_H_ */
